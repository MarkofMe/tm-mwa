TeesMo - Mechanics Web App
=======================

Project Desciption

### Technologies

| Area                      |  Technologies                             |
|:--------------------------|:------------------------------------------|
| Hosting Platforms         | Azure                                         |
| Development Platforms     | Linux, Windows, OSX                       |
| Build Automation          | [FAKE](http://fsharp.github.io/FAKE/)     |
| Unit Testing              | [xUnit](https://github.com/xunit/xunit)            |
| Package Formats           | Nuget packages                            |
| Documentation Authoring   | Markdown, HTML and F# Literate Scripts    |

### Initializing

    Open Cammand Line and Navigate to the tm-mwa directory.
    Type ".\.paket\paket.exe install" press enter
    This will install all of the packages they project uses.
    Once done type " .\build.cmd" press enter
    This will build the project.

    If theres an error, try opening the solution and building from inside Visual Studio.

    If not building, contact me.

### Link

https://teesmo-mechanicswebapp.azurewebsites.net/webpages/Mechanics.html#!/login

