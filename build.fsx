// include Fake lib
#r @"packages\FAKE\tools\FakeLib.dll"
#r @"packages\build\Chessie\lib\net40\Chessie.dll"
#r @"packages\build\Paket.Core\lib\net45\Paket.Core.dll"
open Fake
open Fake.UserInputHelper
open System
open System.IO
#if MONO
#else
#load "packages/build/SourceLink.Fake/tools/Fake.fsx"
open SourceLink
#endif

let project = "MechanicWebApp"

let summary = "Front end for the Mechanics."

let description = ""

let authors = ["mark.leonard406@hotmail.com"]

let tags = "MechanicWebApp"

let solutionFile = "MechanicWebApp.sln"

open Paket
open Paket.Xml
open System.Xml

let setProjectAttribute attribute innerText (propertyGroup:Xml.XmlNode) =
    let nodes = propertyGroup.ChildNodes |> Seq.cast<XmlNode> |> Seq.filter(fun node -> node.Name =  attribute )
    nodes |> Seq.iter(fun node -> propertyGroup.RemoveChild(node) |> ignore)
    let attribute = propertyGroup.OwnerDocument.CreateElement(attribute, Constants.ProjectDefaultNameSpace)
    match innerText with
    | null -> ()
    | _ -> attribute.InnerText <- innerText
    propertyGroup.AppendChild attribute |> ignore
    propertyGroup

let targetFrameworkVersion = setProjectAttribute "TargetFrameworkVersion" "v4.6.1"

let targetFSharpCoreVersion = setProjectAttribute "TargetFSharpCoreVersion" "4.4.0.0"

let prefer64Bit = setProjectAttribute "Prefer32Bit" "false"

let treatWarningsAsErrors = setProjectAttribute "TreatWarningsAsErrors" "false"

let warningsAsErrors = setProjectAttribute "WarningsAsErrors" null

let otherFlags = setProjectAttribute "OtherFlags" ""

let requireAutomaticBindingRedirects = setProjectAttribute "AutoGenerateBindingRedirects" "true"

let requiredCommon = targetFrameworkVersion >> targetFSharpCoreVersion >> treatWarningsAsErrors >> warningsAsErrors >> otherFlags >> requireAutomaticBindingRedirects >> ignore

let requiredEXE = prefer64Bit >> requiredCommon

let requiredConfig projectPath =
    let project = ProjectFile.TryLoad projectPath
    match project with
    | None -> ()
    | Some p ->
        let propertyGroup = getNodes "PropertyGroup" p.ProjectNode
        match p.OutputType with
        | Paket.ProjectOutputType.Exe -> propertyGroup |> List.iter requiredEXE
        | Paket.ProjectOutputType.Library -> propertyGroup |> List.iter requiredCommon
        p.Save(true)

// Makes sure all proj files have required assemblies
Target "RequiredProjConfig" (fun _ ->
  let fsProjs =  !! "*/**/*.*proj"
  fsProjs |> Seq.iter requiredConfig
)

Target "CopyBinaries" (fun _ ->
    !! "src/**/*.??proj"
    |> Seq.map (fun f -> ((System.IO.Path.GetDirectoryName f) @@ "bin", "bin" @@ (System.IO.Path.GetFileNameWithoutExtension f)))
    |> Seq.iter (fun (fromDir, toDir) -> CopyDir toDir fromDir (fun _ -> true))
)

Target "Clean" (fun _ ->
    CleanDirs ["bin"; "temp"]
)

Target "Build" (fun _ ->
    !! solutionFile
    |> MSBuildRelease "" "Rebuild"
    |> ignore
)

// Default target
Target "Default" (fun _ ->
    trace "Hello World from FAKE"
)

Target "All" DoNothing

"Clean"
  ==> "RequiredProjConfig"
  ==> "Build"
  ==> "CopyBinaries"
  ==> "All"

// start build
RunTargetOrDefault "All"