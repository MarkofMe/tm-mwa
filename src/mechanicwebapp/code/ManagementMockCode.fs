namespace ManagementMock

open System
open FSharp.Core

module ManagementMock =

    type managementData = {
                        mutable UniqueId : string
                        mutable UserName : string
                        mutable Password : string
                        mutable FirstName : string
                        mutable MiddleName : string
                        mutable LastName : string
                        mutable DateofBirth : string
                        mutable Authorised : bool
                        mutable Management : bool
                        mutable Active : bool
                    }
    
    let private emptyMockManagementData = {
                                            UniqueId = ""
                                            UserName = ""
                                            Password = ""
                                            FirstName = ""
                                            MiddleName = ""
                                            LastName = ""
                                            DateofBirth = ""
                                            Authorised = false
                                            Management = false
                                            Active = false
                                          }
    
    type managementLoginData = {
                        UniqueId : string
                        UserName : string
                        Password : string
                        FirstName : string
                        MiddleName : string
                        LastName : string
                        DateofBirth : string
                        Authorised : bool
                        Management : bool
                        Active : bool
                        Status : string
                    }
    
    let mutable mockManagementData = 
        [|
            {UniqueId = "123"
             UserName = "User1"
             Password = "Password1"
             FirstName = "Mark"
             MiddleName = "Joseph"
             LastName = "Leonard"
             DateofBirth = "29/04/1995"
             Authorised = true
             Management = true
             Active = true};
            {UniqueId = "456"
             UserName = "User2"
             Password = "Password2"
             FirstName = "Darien"
             MiddleName = ""
             LastName = "Goodwin-Madden"
             DateofBirth = "04/07/1996"
             Authorised = true
             Management = true
             Active = true};
            {UniqueId = "789"
             UserName = "User3"
             Password = "Password3"
             FirstName = "Ross"
             MiddleName = ""
             LastName = "Williams"
             DateofBirth = "21/08/1996"
             Authorised = true
             Management = false
             Active = true};
            {UniqueId = "147"
             UserName = "User4"
             Password = "Password4"
             FirstName = "Sean"
             MiddleName = ""
             LastName = "Johnson"
             DateofBirth = "14/04/1997"
             Authorised = false
             Management = false
             Active = true};
            {UniqueId = "258"
             UserName = "User5"
             Password = "Password5"
             FirstName = "Josh"
             MiddleName = ""
             LastName = "Percival"
             DateofBirth = "13/08/1996"
             Authorised = false
             Management = false
             Active = false}
        |]

    let getAccount(login : string[]) =

        let uName = login.[0]
        let pass = login.[1]
        (
        try
            (mockManagementData |> Array.map (fun row -> 
                                          match row.UserName.Equals(uName) && row.Password.Equals(pass) with
                                          | true -> 
                                                   {
                                                    UniqueId = row.UniqueId
                                                    UserName = row.UserName
                                                    Password = row.Password
                                                    FirstName = row.FirstName
                                                    MiddleName = row.MiddleName
                                                    LastName = row.LastName
                                                    DateofBirth = row.DateofBirth
                                                    Authorised = row.Authorised
                                                    Management = row.Management
                                                    Active = row.Active
                                                    Status = "OK"
                                                    } |> Some
                                          |_ -> None
                                            ) |> Array.choose id).[0]
        with ex ->
            try
                (mockManagementData |> Array.map (fun row -> 
                                          match row.UserName.Equals(uName) with
                                          | true -> 
                                                   {
                                                    UniqueId = row.UniqueId
                                                    UserName = row.UserName
                                                    Password = row.Password
                                                    FirstName = row.FirstName
                                                    MiddleName = row.MiddleName
                                                    LastName = row.LastName
                                                    DateofBirth = row.DateofBirth
                                                    Authorised = row.Authorised
                                                    Management = row.Management
                                                    Active = row.Active
                                                    Status = "PASSWORD"
                                                    } |> Some
                                          |_ -> None
                                            ) |> Array.choose id).[0]
            with ex ->
                    {
                        UniqueId = ""
                        UserName = ""
                        Password = ""
                        FirstName = ""
                        MiddleName = ""
                        LastName = ""
                        DateofBirth = ""
                        Authorised = false
                        Management = false
                        Active = false
                        Status = "NONE"
                    }
                )
