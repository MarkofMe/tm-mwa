namespace StaffProfileMock

open System
open System.Net
open FSharp.Core
open Newtonsoft.Json

module StaffProfileMock =

    type authorisation = {
                            Authorisation : string
                            }

    type staffData = {
                        mutable UniqueId : string
                        mutable FirstName : string
                        mutable MiddleName : string
                        mutable LastName : string
                        mutable DateofBirth : string
                        mutable Authorised : authorisation[]
                        mutable Active : bool
                    }
    
    let private emptyAuthorisation = {
                                        Authorisation = ""
                                        }
    
    type UidAuth = {
                    mutable UniqueId : string
                    mutable Authorised : string[]
                  }

    let private emptyMockStaffData = {
                                            UniqueId = ""
                                            FirstName = ""
                                            MiddleName = ""
                                            LastName = ""
                                            DateofBirth = ""
                                            Authorised = [|emptyAuthorisation|]
                                            Active = false
                                          }
    
    type displayStaffMockData = {
                                mutable UniqueId : string
                                mutable FirstName : string
                                mutable MiddleName : string
                                mutable LastName : string
                                mutable DateofBirth : string
                                mutable Active : bool
                                }
    
    let emptyDisplayStaffMockData = {
                                     UniqueId = ""
                                     FirstName = ""
                                     MiddleName = ""
                                     LastName = ""
                                     DateofBirth = ""
                                     Active = false
                                     }

    type cutDownStaffData = {
                        mutable UniqueId : string
                        mutable FirstName : string
                        mutable LastName : string
                        mutable DateofBirth : string
                    }

    let private emptyMockCutDownStaffData = {
                                            UniqueId = ""
                                            FirstName = ""
                                            LastName = ""
                                            DateofBirth = ""
                                          }
    
    let authorisationData = [|
                                {Authorisation = "CanPurchaseFromThirdParty"}
                                {Authorisation = "CanSendMessages"}
                                {Authorisation = "CanViewCustomerProfile"}
                                {Authorisation = "CanViewCustomerOrderHistory"}
                                {Authorisation = "CanViewCustomerMessageHistory"}
                                {Authorisation = "CanViewStockLevel"}
                                {Authorisation = "CanSendPendingInvoices"}
                                {Authorisation = "CanEditCustomerPerms"}
                                {Authorisation = "CanEditProducts"}
                                {Authorisation = "CanViewProductPriceHistory"}
                                |] |> Array.map(fun i -> {Authorisation = i.Authorisation})
    


    let mutable mockStaffData = 
        [|
            {UniqueId = "123"
             FirstName = "Mark"
             MiddleName = "Joseph"
             LastName = "Leonard"
             DateofBirth = "29/04/1995"
             Authorised = [|{Authorisation = "CanPurchaseFromThirdParty"}
                            {Authorisation = "CanSendMessages"}
                            {Authorisation = "CanViewCustomerProfile"}
                            {Authorisation = "CanViewCustomerOrderHistory"}|]
             Active = true};
            {UniqueId = "456"
             FirstName = "Darien"
             MiddleName = ""
             LastName = "Goodwin-Madden"
             DateofBirth = "04/07/1996"
             Authorised = [|{Authorisation = "CanPurchaseFromThirdParty"}
                            {Authorisation = "CanSendMessages"}
                            {Authorisation = "CanViewCustomerProfile"}
                            {Authorisation = "CanViewCustomerOrderHistory"}|]
             Active = true};
            {UniqueId = "789"
             FirstName = "Ross"
             MiddleName = ""
             LastName = "Williams"
             DateofBirth = "21/08/1996"
             Authorised = [|{Authorisation = "CanPurchaseFromThirdParty"}
                            {Authorisation = "CanSendMessages"}
                            {Authorisation = "CanViewCustomerProfile"}
                            {Authorisation = "CanViewCustomerOrderHistory"}|]
             Active = true};
            {UniqueId = "147"
             FirstName = "Sean"
             MiddleName = ""
             LastName = "Johnson"
             DateofBirth = "14/04/1997"
             Authorised = [|emptyAuthorisation|]
             Active = true};
            {UniqueId = "258"
             FirstName = "Josh"
             MiddleName = ""
             LastName = "Percival"
             DateofBirth = "13/08/1996"
             Authorised = [|emptyAuthorisation|]
             Active = false}
        |]

    let getDisplayMockStaff() =
        try
            mockStaffData |> Array.map (fun row -> 
                                          match row.Active with
                                          | true -> 
                                                   {
                                                    UniqueId = row.UniqueId
                                                    FirstName = row.FirstName
                                                    LastName = row.LastName
                                                    DateofBirth = row.DateofBirth
                                                    } |> Some
                                          |_ -> None
                                            ) |> Array.choose id
        with ex ->
            [|emptyMockCutDownStaffData|]

    let filterMockStaff (uID : string) =
        let data =
            mockStaffData |> Seq.map (fun row -> match ((row.UniqueId).ToString()).Equals(uID) with
                                                      | true -> {
                                                                  UniqueId = row.UniqueId
                                                                  FirstName = row.FirstName
                                                                  MiddleName = row.MiddleName
                                                                  LastName = row.LastName
                                                                  DateofBirth = row.DateofBirth
                                                                  Active = row.Active
                                                                 } |> Some
                                                      | _-> None
                          )
        (data |> Seq.toArray |> Array.choose id)

    let getMockStaffDetails (staffUID : string) =
        try
            filterMockStaff staffUID
        with ex ->
            [|emptyDisplayStaffMockData|]

    let filterStaffAuth (uID : string) =
        let data = mockStaffData |> Seq.map (fun row -> match ((row.UniqueId).ToString()).Equals(uID) with
                                                        | true -> row.Authorised |> Some
                                                        | _-> None
                          )
        (data |> Seq.toArray |> Array.choose id).[0]

    let getMockStaffAuthorisations (staffUID : string) =
        try
            filterStaffAuth staffUID
        with ex ->
            [|emptyAuthorisation|]
    
    let filterMockStaffFullData (uID : string) =
        let data =
            mockStaffData |> Seq.map (fun row -> match ((row.UniqueId).ToString()).Equals(uID) with
                                                      | true -> {
                                                                  UniqueId = row.UniqueId
                                                                  FirstName = row.FirstName
                                                                  MiddleName = row.MiddleName
                                                                  LastName = row.LastName
                                                                  DateofBirth = row.DateofBirth
                                                                  Authorised = row.Authorised
                                                                  Active = row.Active
                                                                 } |> Some
                                                      | _-> None
                          )
        (data |> Seq.toArray |> Array.choose id)

    //let changeAuthMockStaff (id : string)( auths : string[]) = 
    let changeAuthMockStaff (data : string[]) = 
        try 
            let deserializedData = JsonConvert.DeserializeObject<UidAuth>(data.[0])

            let auths = deserializedData.Authorised |> Seq.map(fun i -> {Authorisation = i}) |> Seq.toArray

            let mutable staff = (filterMockStaffFullData deserializedData.UniqueId).[0]
            staff.Authorised <- auths
        
            let i = mockStaffData |> Array.findIndex (fun i -> i.UniqueId.Equals(staff.UniqueId))

            Array.set mockStaffData i staff
            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError