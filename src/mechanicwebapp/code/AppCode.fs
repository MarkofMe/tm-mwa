namespace App

open System
open System.Data
open System.Data.Linq
open System.Data.SqlClient
open System.Web.Http
open Microsoft.FSharp.Data.TypeProviders
open Microsoft.FSharp.Linq
open FSharp.Data
open FSharp.Core
open DataLayer.DataLayer
open System.IO
open Microsoft.WindowsAzure.Storage.Table

module App =
    0 |> ignore
