var Mechanics = angular.module('Mechanics', ['ngRoute']);

Mechanics.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/login', {
            templateUrl: 'Views/login.html',
            controller: 'loginController'
        })
        .when('/manageServices', {
            templateUrl: 'Views/serviceManage.html',
            controller: 'manageServicesController'
        })
        .when('/previousService', {
            templateUrl: 'Views/servicePrevious.html',
            controller: 'ServicePreviousController'
        })
        .when('/viewService', {
            templateUrl: 'Views/serviceView.html',
            controller: 'viewServiceController'
        })
        .when('/addServiceLog', {
            templateUrl: 'Views/serviceLogAdd.html',
            controller: 'addServiceLogController'
        })
        .when('/carView', {
            templateUrl: 'Views/carView.html',
            controller: 'carViewController'
        })
        .when('/expenses', {
            templateUrl: 'Views/expenses.html',
            controller: 'expensesController'
        })
        .when('/expensesFile', {
            templateUrl: 'Views/expensesFile.html',
            controller: 'expensesFileController'
        })
        .when('/myAccount', {
            templateUrl: 'Views/myAccount.html',
            controller: 'myAccountController'
        })
        .when('/editAccount', {
            templateUrl: 'Views/myAccountEdit.html',
            controller: 'editAccountController'
        })
        .when('/apologies', {
            templateUrl: 'Views/apologiesView.html'
        })
        .otherwise({
            redirectTo: '/login'
        });
}]);

Mechanics.controller('loginController', function ($scope, $http) {

    $("#logres").text('Enter UserName and Password');

    $("#login-form").validate(); //Jquery validate

    $("#usernameinput").rules('add', {
        required: true
    });

    $("#passwordinput").rules('add', {
        required: true
    });

    $("#login-form").validate();

    $('#loginButton').click(function () {

        var uName = $("#usernameinput").val();
        var pass = $("#passwordinput").val();

        $.ajax({
            method: "POST",
            url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/login',
            dataType: 'Json',
            data: '{"StaffUserName": "' + uName + '","StaffPassword": "' + pass + '"}',
            success: function (userData) {
                console.log(userData);
                if (userData[0].status === "OK") {
                    if (userData[0].position === "Admin" || userData[0].position === "Mechanic" || userData[0].position === "Management") {
                        $("#logres").text("");
                        $("#logres").text('UserName and Password good');

                        var secretKey = makeid();
                        var encryptedPassword = CryptoJS.AES.encrypt(pass, secretKey);

                        sessionStorage.setItem("userData", JSON.stringify(userData));
                        sessionStorage.setItem("password", encryptedPassword);
                        sessionStorage.setItem("secretKey", secretKey);

                        console.log(userData);
                        window.location.href = '#!/manageServices';
                    } else {
                        alert("Unauthorised");
                    }
                    
                }
                else if (userData[0].status === "PASSWORD") {
                    $("#logres").text("");
                    $("#logres").text('Incorrect Password');

                }
                else if (userData[0].status === "INACTIVE") {
                    $("#logres").text("");
                    $("#logres").text('Account is Inactive');

                }
                else if (userData[0].status === "WRONG") {
                    $("#logres").text("");
                    $("#logres").text('Account does not exist');

                }
            }
        });
    });

});

Mechanics.controller('manageServicesController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        console.log(userData);

        if (userData[0].position === "Admin") {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/All',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
                success: function (results) {
                    console.log(results);
                    $('#manageServicesTable').empty();
                    $('#manageServicesTable').append('<tr><th>Service ID</th><th>Car</th><th>Dealership</th><th>Date</th><th>View</th></tr>');


                    $.each(results, function (index, t) {
                        $('#manageServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/viewService#type=ServiceID&serviceID=' + t.serviceBookingID + '">View Service</a></td></tr>');
                    });
                }
            });
        }
        else {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/AllByDealership',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","Dealership": "' + userData[0].dealershipName + '"}',
                success: function (results) {
                    console.log(results);
                    $('#manageServicesTable').empty();
                    $('#manageServicesTable').append('<tr><th>Service ID</th><th>Car</th><th>Dealership</th><th>Date</th><th>View</th></tr>');
                    
                    $.each(results, function (index, t) {
                        $('#manageServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/viewService#type=ServiceID&serviceID=' + t.serviceBookingID + '">View Service</a></td></tr>');
                    });
                }
            });
        }
    }
});

Mechanics.controller('ServicePreviousController', function ($scope, $http) {
    var existData = "";
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Admin") {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/AllCompleted',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
                success: function (results) {
                    console.log(results);
                    $('#previousServicesTable').empty();
                    $('#previousServicesTable').append('<tr><th>Service ID</th><th>Car</th><th>Dealership</th><th>Date</th><th></th></tr>');
                    
                    $.each(results, function (index, t) {
                        $('#previousServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/viewService#type=ServiceID&serviceID=' + t.serviceBookingID + '">View Service</a></td></tr>');
                    });
                }
            });
        }
        else {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/AllByDealershipCompleted',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","Dealership": "' + userData[0].dealershipName + '"}',
                success: function (results) {
                    console.log(results);
                    $('#previousServicesTable').empty();
                    $('#previousServicesTable').append('<tr><th>Service ID</th><th>Car</th><th>Dealership</th><th>Date</th><th>View</th></tr>');
                    
                    $.each(results, function (index, t) {
                        $('#previousServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/viewService#type=ServiceID&serviceID=' + t.serviceBookingID + '">View Service</a></td></tr>');
                    });
                }
            });
        }
    }
});

Mechanics.controller('viewServiceController', function ($scope, $http) {
    var existData = "";
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Sales") {
            alert("Not Authorised");
            window.location.href = '#!/sales';
        }
        else {
            var location_vars = [];
            var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
            location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
            for (i = 0; i < location_vars_temp.length; i++) {
                location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
                location_vars.push(location_var_key_val[1]);
            }

            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/ByID',
                dataType: 'Json',
                data: '{"ServiceBookingID" : ' + location_vars[1] + '}',
                success: function (results) {
                    console.log(results[0]);

                    $('#viewServiceContainerDiv').empty();
                    if (results[0].completed === false) {
                        $('#viewServiceContainerDiv').append('<p>Car: <a id="viewCar-' + results[0].carID + '" href="#!/carView#type=viewCar&carID=' + results[0].carID + '">' + results[0].car + '</a></p>\
                            <p>Customer: '+ results[0].customerName + '</p>\
                            <p>Location: '+ results[0].dealershipName + '</p>\
                            <p>Date: '+ (results[0].dateBooked) + '</p><br/><br/>\
                            <a href="#!/addServiceLog#type=insert&serviceID='+ results[0].serviceBookingID + '" style="padding-right:30px;">Add to Service Log</a><a id="completeService-' + results[0].serviceBookingID + '">Complete Service</a><br/><br/>\
                            <table id="viewService-table"></table>');

                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/ServiceLogsByServiceBookingID',
                            dataType: 'Json',
                            data: '{  "ServiceBookingID": ' + results[0].serviceBookingID + '}',
                            success: function (results) {
                                console.log(results);
                                $('#viewService-table').empty();
                                if (results.length === 0) {
                                    $('#viewServiceContainerDiv').append('<p>No Logs Available</p>');
                                } else {
                                    $('#viewService-table').append('<tr><th>#</th><th>Staff Member</th><th>Process</th><th>Time (hours)</th><th>Notes</th><th>Amount</th><th>Additional Cost</th><th>Edit</th></tr>');

                                    $.each(results, function (index, t) {
                                        if (t.serviceProcessStatus === "Completed"){
                                            $('#viewService-table').append('<tr><td>' + index + '</td><td>' + t.staffName + '</td><td>' + t.serviceProcess + '</td><td>' + t.time + '</td><td>' + t.notes + '</td><td>' + t.amount + '</td><td>' + t.additionalCost + '</td><td>Completed</td></tr>');
                                        } else {
                                            $('#viewService-table').append('<tr><td>' + index + '</td><td>' + t.staffName + '</td><td>' + t.serviceProcess + '</td><td>' + t.time + '</td><td>' + t.notes + '</td><td>' + t.amount + '</td><td>' + t.additionalCost + '</td><td><a href="#!/addServiceLog#type=update&logID=' + t.serviceLogID + '&serviceBookingID=' + t.serviceBookingID + '">Complete Log</a></td></tr>');
                                        }
                                        
                                    });
                                }
                            }
                        });

                        $('#completeService-' + results[0].serviceBookingID).click(function () {
                            console.log("Complete Service");

                            var a = '{"UserName" : "", "Password" : "","ServiceBookingID" : 0,"DealershipName" : 0,"CustomerID" :0,"StaffID" : 0,}'
                            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/InsertServiceCompleted',
                                dataType: 'Json',
                                data: '{"UserName" : "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","ServiceBookingID" : ' + results[0].serviceBookingID +
                                ',"DealershipName" : "' + results[0].dealershipName + '","CustomerID" :' + results[0].customerID + ',"StaffID" : ' + userData[0].staffID+'}',
                                success: function (results) { window.location.href = '#!/manageServices'; }
                            });
                        });

                    } else {
                        $('#viewServiceContainerDiv').append('<p>Car: <a id="viewCar-' + results[0].carID + '" href="#!/carView#type=viewCar&carID=' + results[0].carID + '">' + results[0].car + '</a></p>\
                            <p>Customer: '+ results[0].customerName + '</p>\
                            <p>Location: '+ results[0].dealershipName + '</p>\
                            <p>Date: '+ (results[0].dateBooked) + '</p><br/><br/>\
                            <table id="viewService-table"></table>');

                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/CompletedServiceLogsByServiceBookingID',
                            dataType: 'Json',
                            data: '{  "ServiceBookingID": ' + results[0].serviceBookingID + '}',
                            success: function (results) {
                                console.log(results);
                                $('#viewService-table').empty();
                                if (results.length === 0) {
                                    $('#viewServiceContainerDiv').append('<p>No Logs Available</p>');
                                } else {
                                    $('#viewService-table').append('<tr><th>#</th><th>Staff Member</th><th>Process</th><th>Time (hours)</th><th>Notes</th><th>Amount</th><th>Additional Cost</th></tr>');

                                    $.each(results, function (index, t) {
                                        $('#viewService-table').append('<tr><td>' + index + '</td><td>' + t.staffName + '</td><td>' + t.serviceProcess + '</td><td>' + t.time + '</td><td>' + t.notes + '</td><td>' + t.amount + '</td><td>' + t.additionalCost + '</td></tr>');
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
    }
});

Mechanics.controller('addServiceLogController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }

        $("#addServiceForm").validate({
            rules: {
                additionalCost: {
                    number: true,
                    maxlength: 7,
                    required: false
                },
                notes: {
                    maxlength: 300,
                    required: true
                }
            },
            messages: {
                additionalCost: {
                    required: "Please specify an amount.",
                    number: "Must be a number."
                },
                notes: {
                    required: "Please specify the nature of the service log."
                }
            }
        });

        if (location_vars[0] === "insert") {
            $('#ServiceLog-button').click(function () {
                if ($("#addServiceForm").valid() === false) {
                    alert("Invalid Input");
                }
                else {
                    var serviceBookingID = location_vars[1];
                    var process = $('#addService-processType').find(":selected").val();
                    var notes = $('#addService-Notes').val();
                    var additionalCost;

                    if ($('#addService-additionalCosts').val() === "") {
                        additionalCost = "00.00";
                    } else {
                        additionalCost = $('#addService-additionalCosts').val();
                    }

                    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                    $.ajax({
                        method: "POST",
                        url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/InsertServiceLog',
                        dataType: 'Json',
                        data: '{"UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","ServiceBookingID": ' + serviceBookingID
                        + ',"StaffID": ' + userData[0].staffID + ', "Process": "' + process + '", "Notes": "' + notes + '","AdditionalCost": ' + parseFloat(additionalCost) + ' }',
                        success: function (results) {
                            window.location.href = '#!/viewService#type=ServiceID&serviceID=' + serviceBookingID;
                        }
                    });
                }
            });
        }
        else if (location_vars[0] === "update") {
            $("#addService-processType").prop('disabled', true);
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/ByServiceLogID',
                dataType: 'Json',
                data: '{  "ServiceLogID": ' + location_vars[1] + '}',
                success: function (results) {
                    console.log(results);
                    $('#addService-processType').val(results[0].serviceProcess);
                    $('#addService-Notes').val(results[0].notes);
                    $('#addService-additionalCosts').val(results[0].additionalCost);
                }
            });


            $('#ServiceLog-button').click(function () {
                if ($("#addServiceForm").valid() === false) {
                    alert("Invalid Input");
                }
                else {
                    var serviceLogID = location_vars[1];
                    var notes = $('#addService-Notes').val();
                    var additionalCost;

                    if ($('#addService-additionalCosts').val() === "") {
                        additionalCost = "00.00";
                    } else {
                        additionalCost = $('#addService-additionalCosts').val();
                    }

                    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                    $.ajax({
                        method: "POST",
                        url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/CompleteServiceLog',
                        dataType: 'Json',
                        data: '{"UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","ServiceLogID": ' + serviceLogID
                        + ', "Notes": "' + notes + '","AdditionalCost": ' + parseFloat(additionalCost) + ' }',
                        success: function (results) {
                            window.location.href = '#!/viewService#type=ServiceID&serviceID=' + location_vars[2];
                        }
                    });
                }
            });
        }
    }
});

Mechanics.controller('carViewController', function ($scope, $http, UserData) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }

        console.log(location_vars);

        $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/CarDetails',
            dataType: 'Json',
            data: '{"carID" : ' + location_vars[1] + '}',
            success: function (results) {
                console.log(results);

                $('#viewCar-title').text(results[0].make + " " + results[0].model);
                
                $('#viewCarContainerDiv').empty();
                $('#viewCarContainerDiv').append('<div style="height:410px"><div class="viewCarImageDiv" id="viewCarImageDiv-' + results[0].carID + '"><img class="view-imageOutput" id="view-imageOutput' + results[0].carID + '"></div>\
                <div class="testDriveDiv" id= "testDriveDiv' + results[0].carID + '">\
                </div ></div > <br />\
                <div class="viewCarDataDiv" id="viewCarDataDiv' + results[0].carID + '"><ul>' +
                    '<li class="carView_list" id="carView_list_year">' + results[0].year + '</li >' +
                    '<li class="carView_list" id="carView_list_bodyType">' + results[0].bodyType + '</li>' +
                    '<li class="carView_list" id="carView_list_miles">' + results[0].miles + 'miles</li>' +
                    '<li class="carView_list" id="carView_list_engineSize">' + results[0].engineSize + 'L</li>' +
                    '<li class="carView_list" id="carView_list_gearbox">' + results[0].gearBoxType + '</li>' +
                    '<li class="carView_list" id="carView_list_fuel">' + results[0].fuelType + '</li>' +
                    '</ul >' +
                    '<ul>' +
                    '<li class="carView_list" id="carView_list_colour">' + results[0].colour + '</li>' +
                    '<li class="carView_list" id="carView_list_doors">' + results[0].doors + ' doors</li>' +
                    '<li class="carView_list" id="carView_list_sixtyTime">' + results[0].sixtyTime + 's</li>' +
                    '<li class="carView_list" id="carView_list_horsePower">' + results[0].horsePower + 'bhp</li>' +
                    '<li class="carView_list" id="carView_list_driveTrain">' + results[0].driveTrainType + '</li>' +
                    '<li class="carView_list" id="carView_list_co2">' + results[0].co2 + 'g/km</li>' +
                    '</ul></div>');

                var output2 = document.getElementById('view-imageOutput' + results[0].carID);
                output2.src = results[0].imageString;
                output2.style.height = '400px';
                output2.style.width = '550px';
            }
        });
    }
});

Mechanics.controller('expensesController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Admin") {
            adminExpenses(userData);
        }
        else {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/ByStaffID',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","StaffID": "' + userData[0].staffID + '"}',
                success: function (results) {
                    console.log(results);
                    $('#expensesTable').empty();
                    $('#expensesTable').append('<tr><th>#</th><th>Expense Type</th><th>Amount</th><th>Date</th><th>Notes</th><th>Status</th></tr>');

                    $.each(results, function (index, t) {
                        $('#expensesTable').append('<tr><td>' + index + '</td><td>' + t.expenseType + '</td><td>' + t.amount + '</td><td>' + (t.date.split('T'))[0] + '</td><td>' + t.notes + '</td><td>' + t.status + '</td></tr>');

                    });
                }
            });
        }
    }
});

function adminExpenses(userData) {
    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
    $.ajax({
        method: "POST",
        url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/Expenses',
        dataType: 'Json',
        data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
        success: function (results) {
            console.log(results);
            $('#expensesTable').empty();
            $('#expensesTable').append('<tr><th>#</th><th>Expense Type</th><th>Amount</th><th>Staff Member</th><th>Date</th><th>Notes</th><th>Status</th></tr>');

            $.each(results, function (index, t) {

                if (t.status === "Pending") {
                    $('#expensesTable').append('<tr><td>' + index + '</td><td>' + t.expenseType + '</td><td>' + t.amount + '</td><td>' + t.staffName + '</td><td>' + (t.date.split('T'))[0] + '</td><td>' + t.notes + '</td><td>' + t.status + '</td><td><a id="approveExpense-' + t.expenseID + '">Approve Expense</a></td><td><a id="rejectExpense-' + t.expenseID + '">Reject Expense</a></td></tr>');

                    $('#approveExpense-' + t.expenseID).click(function () {
                        var accept = confirm("Do you want to continue?");
                        if (accept) {
                            var decrypted2 = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/ApproveExpense',
                                dataType: 'Json',
                                data: '{"UserName" : "' + userData[0].staffUserName + '","Password": "' + decrypted2.toString(CryptoJS.enc.Utf8) + '","ExpenseID": ' + t.expenseID + '}',
                                success: function (results) { adminExpenses(userData) }
                            });
                        }
                    });

                    $('#rejectExpense-' + t.expenseID).click(function () {
                        var accept = confirm("Do you want to continue?");
                        if (accept) {
                            var decrypted2 = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/RejectExpense',
                                dataType: 'Json',
                                data: '{"UserName" : "' + userData[0].staffUserName + '","Password": "' + decrypted2.toString(CryptoJS.enc.Utf8) + '","ExpenseID": ' + t.expenseID + '}',
                                success: function (results) { adminExpenses(userData) }
                            });
                        }
                    });
                }
                else {
                    $('#expensesTable').append('<tr><td>' + index + '</td><td>' + t.expenseType + '</td><td>' + t.amount + '</td><td>' + t.staffName + '</td><td>' + (t.date.split('T'))[0] + '</td><td>' + t.notes + '</td><td>' + t.status + '</td><td></td><td></td></tr>');
                }
            });
        }
    });
};

Mechanics.controller('expensesFileController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {

        $("#fileExpenseContainerDiv").validate({
            rules: {
                amount: {
                    number: true,
                    maxlength: 7,
                    required: true
                },
                notes: {
                    maxlength: 200,
                    required: true
                }
            },
            messages: {
                amount: {
                    required: "Please specify an amount.",
                    number: "Must be a number."
                },
                notes: {
                    required: "Please specify the nature of the expense."
                }
            }
        });

        $('#fileExpense-Button').click(function () {
            if ($("#fileExpenseContainerDiv").valid() === false) {
                alert("Invalid Input");
            }
            else {
                var type = $('#fileExpense-expenseType').find(":selected").val();
                var amount = $('#fileExpense-Amount').val();
                var notes = $('#fileExpense-Notes').val();

                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/InsertExpense',
                    dataType: 'Json',
                    data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) +
                    '","ExpenseType": "' + type + '","Amount": ' + parseFloat(amount) + ',"StaffID": ' + userData[0].staffID + ',"Notes": "' + notes + '"}',

                    success: function (results) {
                        window.location.href = '#!/expenses';
                    }
                });
            }
        });
    }
});

Mechanics.controller('myAccountController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        console.log(userData[0]);
        $('#staffAccount-userName').val(userData[0].staffUserName);
        $('#staffAccount-firstName').val(userData[0].firstName);
        $('#staffAccount-lastName').val(userData[0].lastName);
        $('#staffAccount-email').val(userData[0].email);
        $('#staffAccount-number').val(userData[0].houseNumber);
        $('#staffAccount-address1').val(userData[0].address1);
        $('#staffAccount-address2').val(userData[0].address2);
        $('#staffAccount-city').val(userData[0].town);
        $('#staffAccount-postcode').val(userData[0].postcode);
        $('#staffAccount-position').val(userData[0].position);
        $('#staffAccount-salary').val(userData[0].salary);
        $('#staffAccount-dealership').val(userData[0].dealershipName);

    }
});

Mechanics.controller('editAccountController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        $("#editAccountDiv").validate({
            rules: {
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                },
                dateofBirth: {
                    required: true,
                    date: true
                },
                email: {
                    required: true,
                    email: true
                },
                houseNumber: {
                    number: true,
                    minlength: 1,
                    required: true
                },
                address1: {
                    required: true
                },
                address2: {
                    required: false
                },
                city: {
                    required: true
                },
                postCode: {
                    required: true
                }
            },
            messages: {
                firstName: "Please enter your firstname",
                lastName: "Please enter your lastname",
                dateofBirth: {
                    required: "Please enter your date of birth (DD-MM-YYYY)",
                    date: "Please enter your date of birth (DD-MM-YYYY)"
                },
                email: "Please enter a valid email address",
                houseNumber: {
                    required: "Please enter your door number",
                    number: "Please enter your door number",
                    minlength: "Please enter your door number",
                },
                address1: "Please enter the first line of your address, house number included.",
                address2: "Please enter the second line of your address, if you have one.",
                city: "Please enter the name of your town/city",
                postCode: "Please enter your postcode"
            }
        });

        $('#editAccount-firstName').val(userData[0].firstName);
        $('#editAccount-lastName').val(userData[0].lastName);
        $('#editAccount-dateofBirth').val(userData[0].doB)
        $('#editAccount-email').val(userData[0].email);
        $('#editAccount-houseNumber').val(userData[0].houseNumber);
        $('#editAccount-address1').val(userData[0].address1);
        $('#editAccount-address2').val(userData[0].address2);
        $('#editAccount-city').val(userData[0].town);
        $('#editAccount-postCode').val(userData[0].postcode);
        
        $('#editAccount-Button').click(function () {
            if ($("#editAccountDiv").valid() === false) {
                alert("Invalid Input");
            }
            else {
                var firstName = $('#editAccount-firstName').val();
                var LastName = $('#editAccount-lastName').val();
                var dateofBirth = $('#editAccount-dateofBirth').val();
                var email = $('#editAccount-email').val();
                var houseNumber = $('#editAccount-houseNumber').val();
                var address1 = $('#editAccount-address1').val();
                var address2 = $('#editAccount-address2').val();
                var city = $('#editAccount-city').val();
                var postCode = $('#editAccount-postCode').val().toUpperCase();

                var date = new Date(dateofBirth),
                    month = '' + (date.getMonth() + 1),
                    day = '' + date.getDate(),
                    year = date.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                var dateString = [year, month, day].join('-');

                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/UpdateStaff',
                    dataType: 'Json',
                    data: '{"StaffUserName" : "' + userData[0].staffUserName + '","StaffPassword" : "' + decrypted.toString(CryptoJS.enc.Utf8) + '","StaffID" : "' + userData[0].staffID +
                    '","FirstName" : "' + firstName + '","LastName" : "' + LastName + '","DoB" : "' + dateString + '","Email" : "' + email + '","HouseNumber" : ' + parseInt(houseNumber) + ',"Address1" : "' + address1 +
                    '","Address2" : "' + address2 + '","Town" : "' + city + '","Postcode" : "' + postCode + '","Position" : "' + userData[0].position + '","Salary" : ' + userData[0].salary + ',"DealershipName" : "' + userData[0].dealershipName + '"}',
                    success: function (results) {
                        var decrypted2 = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/login',
                            dataType: 'Json',
                            data: '{"StaffUserName": "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted2.toString(CryptoJS.enc.Utf8) + '"}',
                            success: function (userData) {
                                sessionStorage.setItem("userData", JSON.stringify(userData));
                                window.location.href = '#!/myAccount';
                            }
                        });
                    }
                });
            }
        });
    }
});

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[]{};:'@#~,<.>/?!�$%^&*()_-=+";

    for (var i = 0; i < 100; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}