﻿namespace StaffProfileMock.Controllers

open System
open System.Net.Http
open FSharp.Core
open System.Web.Http
open StaffProfileMock


[<RoutePrefixAttribute("StaffProfileMock")>]
type StaffProfileController() =
    inherit ApiController()
    
    [<Route("GetAuth"); HttpGet>]
    member this.GetAuth() =
        System.Diagnostics.Debug.WriteLine("Called data")
        StaffProfileMock.authorisationData

    [<Route("GetMockStaff"); HttpGet>]
    member this.GetStaff () =
        System.Diagnostics.Debug.WriteLine("Called data")
        StaffProfileMock.getDisplayMockStaff()
        
    //member this.ChangeMockStaffAuth (id : string) (auths : string[]) =
    [<Route("ChangeMockStaffAuth")>]
    member this.ChangeStaffAuth (insert : HttpRequestMessage) =
        let content = insert.Content
        let jsonContent = content.ReadAsFormDataAsync().Result
        let mutable data = jsonContent.GetValues(0)
        System.Diagnostics.Debug.WriteLine("Called method insert production data into development")
        StaffProfileMock.changeAuthMockStaff(data)
    
    [<Route("GetMockStaffDetails")>]
    member this.GetCardDetails (id : string) =
        //let content = ac.GetQueryNameValuePairs() |> Seq.map(fun a -> a.Value) |> Seq.toList
        StaffProfileMock.getMockStaffDetails id // content.[0]

    [<Route("GetStaffAuthByID")>]
    member this.GetStaffAuthById (id : string) =
        //let content = ac.GetQueryNameValuePairs() |> Seq.map(fun a -> a.Value) |> Seq.toList
        StaffProfileMock.filterStaffAuth id // content.[0]
    