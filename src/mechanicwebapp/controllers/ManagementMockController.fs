﻿namespace ManagementMock.Controllers

open System
open System.Net.Http
open FSharp.Core
open System.Web.Http
open ManagementMock


[<RoutePrefixAttribute("ManagementMock")>]
type ManagementController() =
    inherit ApiController()

    [<Route("GetAccount"); HttpGet>]
    member this.GetAccount (mess : HttpRequestMessage) =
        let data = mess.GetQueryNameValuePairs() |> Seq.map(fun a -> a.Value) |> Seq.toArray
        System.Diagnostics.Debug.WriteLine("Called data")
        ManagementMock.getAccount(data)
