﻿namespace App.Controllers

open System
open System.Web.Http
open App


[<RoutePrefixAttribute("App")>]
type appController() =
    inherit ApiController()

    [<Route("App"); HttpGet>]
    member this.GetTestData1 () =
        System.Diagnostics.Debug.WriteLine("Called data")
        //Test.get()
        // /Test/TestData
